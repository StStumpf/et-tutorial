# HTTP Get Request auf `http://localhost:3000/`

Anfrage:
```
GET / HTTP/1.1
Host: localhost:3000
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: de,en-US;q=0.7,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: keep-alive
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
If-Modified-Since: Tue, 12 Oct 2021 17:32:45 GMT
If-None-Match: W/"9ff-17c75905558"
```

Antwort:
```
HTTP/1.1 200 OK
X-Powered-By: Express
Accept-Ranges: bytes
Cache-Control: public, max-age=0
Last-Modified: Sun, 19 Sep 2021 11:05:41 GMT
ETag: W/"76e-17bfdbbafdb"
Content-Type: text/html; charset=UTF-8
Content-Length: 1902
Date: Sat, 23 Oct 2021 13:00:23 GMT
Connection: keep-alive
Keep-Alive: timeout=5
```