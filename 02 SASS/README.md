Starten der Applikation mit:

```
node webserver.js
```

Konvertieren von Sass Dateien:

```
npx sass input.scss output.css
```

Autoprefix

```
npx postcss style.css -u autoprefixer -o output.css
```