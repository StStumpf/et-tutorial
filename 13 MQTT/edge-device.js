const mqtt = require("mqtt");

const mqttUrl = "mqtt://localhost";
const mqttTopic = "et20/tutorial/values";

const mqttClient = mqtt.connect(mqttUrl);


/**
   * Funktion, welche beim erfolgreichen Verbinden mit dem MQTT Server abgearbeitet wird.
   */
 mqttClient.on("connect", () => {
  console.log(`MQTT wurde erfolgreich verbunden. Adresse = ${mqttUrl}`);

  setInterval(() => {
    const newValue = {
        timeStamp: new Date(),
        value: Math.random()
    };
    console.log("Sending new value: " + newValue.value)
    mqttClient.publish(mqttTopic, JSON.stringify(newValue));
}, 2000);
});


