const mqtt = require("mqtt");

const mqttUrl = "mqtt://localhost";
const mqttTopic = "et20/tutorial/values";

const mqttClient = mqtt.connect(mqttUrl);

/**
 * Abonniert ein Topic mittels des MQTTClients.
 *
 * @param topic Topic, welches abonniert werden soll.
 */
mqttClientSubscribeToTopic = (topic) => {
  mqttClient.subscribe(topic, {}, (error, granted) => {
    if (granted !== null) {
      console.error(
        `Subscription erfolgreich erstellt. Topic = ${topic}`
      );
    } else {
      console.error("Subscription konnte nicht erstellt werden.");
    }
  });
};


/**
   * Funktion, welche beim erfolgreichen Verbinden mit dem MQTT Server abgearbeitet wird.
   */
mqttClient.on("connect", () => {
  console.log(`MQTT wurde erfolgreich verbunden. Adresse = ${mqttUrl}`);

  // Abonniere ausgewähltes Thema
  mqttClientSubscribeToTopic(mqttTopic);
});

/**
 * Funktion, welche bei einer neuen MQTT Nachricht in dem Subscription Topic ausgeführt wird.
 */
mqttClient.on("message", (topic, message) =>
  console.log(
    "Nachricht via WebSocket erhalten: %s",
    message.toString().trim()
  )
);