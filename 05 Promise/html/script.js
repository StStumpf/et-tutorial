const clockContainer = document.getElementById("clock"); 
const stateContainer = document.getElementById("state"); 
const clockStartContainer = document.getElementById("clock-start"); 
const clockEndContainer = document.getElementById("clock-end"); 
const button = document.getElementById("button");
const buttonReset = document.getElementById("button-reset");

refreshClockContainer = () => {
    clockContainer.innerHTML = new Date().toISOString();
}

setState = (state) => {
    stateContainer.innerHTML = state;
}

setStart = (start) => {
    clockStartContainer.innerHTML = start;
}

setEnd = (end) => {
        clockEndContainer.innerHTML = end;
}

let timeout;
let promise;

startPromise = () => {
    setStart(new Date().toISOString());
    setEnd('-');
    setState('pending');
    promise = new Promise((resolve, reject) => {
        timeout = setTimeout(() => {
            reject();
        }, 5000);
        button.onclick = () => {
            resolve();
            clearTimeout(timeout);
        }
    }).then(() => {
        setState('fulfilled');
        setEnd(new Date().toISOString());
    }).catch(() => {
        setState('rejected');
        setEnd(new Date().toISOString());
    });
}



/**
 * Reset promise
 */
buttonReset.onclick = () => {
    // Clear timeout and promise object
    if (timeout) {
        clearTimeout(timeout);
        timeout = null;
    }

    if (promise) {
        promise = null;
    }

    startPromise();
}

startPromise();
refreshClockContainer();
setInterval(() => {
    refreshClockContainer();
}, 1000);