import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'et-tutorial';
  clock = new Date();

  colorsChangedCount = 0;
  private colorPicked = 0;

  private readonly colorCodes = ['FAEEE0', 'F9CF93', 'F0A500', 'FF7777', 'B1E693'];

  get currentColorCode() {
    return '#' + (
        this.colorCodes[this.colorPicked] ??
        this.colorCodes[this.colorPicked % (this.colorCodes.length - 1)]
    );
  }

  ngOnInit() {
    setInterval(() => {
      this.clock = new Date();
    }, 1000);
  }

  refreshColor() {
    this.colorPicked = (this.colorPicked + 1) % (this.colorCodes.length - 1);
    this.colorsChangedCount += 1;
  }

 
}
