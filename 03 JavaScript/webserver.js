const express = require("express");

const app = express();

/**
 * NEU
 */

app.get("/server-time", (request, response) => {
  response.send(new Date().toISOString());
})


/**
 * 
 */

app.use(express.static("html"));

app.listen(3000, () => {
  console.log("Server gestartet. http://localhost:3000");
});
