import { Colors } from "./colors.js";

const clockContainer = document.getElementById("clock");
const colorContainer = document.getElementById("color");
const colorChangedCountContainer = document.getElementById(
  "color-changed-count"
);
const button = document.getElementById("button");
const colorCodes = Colors.getColors();

let colorsChangedCount = 0;
let colorPicked = 0;

const currentColorCode = () =>
  colorCodes[colorPicked] ?? colorCodes[colorPicked % (colorCodes.length - 1)];

const refreshClockContainer = () => {
  clockContainer.textContent = new Date().toISOString();
};

const refreshColorContainer = () => {
  colorContainer.textContent = `#${currentColorCode()}`;
  colorContainer.style.backgroundColor = `#${currentColorCode()}`;
};

const refreshColorChangedCountContainer = () => {
  colorChangedCountContainer.textContent = `${colorsChangedCount} mal`;
};

/**
 * JQuery Example
 */
const refreshServerClockContainer = (date) => {
  const container = $("#server-clock").get(0);
  console.log(container);
  container.textContent = date;
  console.log("Updated server clock", date);
};

/**
 * Axios Example
 */
const refreshServerClock2Container = (date) => {
  const container = $("#server-clock2").get(0);
  console.log(container);
  container.textContent = date;
  console.log("Updated server clock", date);
};

setInterval(() => {
  refreshClockContainer();
}, 1000);

button.onclick = () => {
  colorPicked = (colorPicked + 1) % (colorCodes.length - 1);
  refreshColorContainer();
  colorsChangedCount += 1;
  refreshColorChangedCountContainer();
};

const init = () => {
  refreshClockContainer();
  refreshColorContainer();
  refreshColorChangedCountContainer();

  $
  .get("http://localhost:3000/server-time", (response) => {
    refreshServerClockContainer(response);
  });

  axios
    .get("http://localhost:3000/server-time")
    .then((response) => {
      refreshServerClock2Container(response.data);
    });
};

init();
