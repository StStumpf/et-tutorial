export class Colors {
    static getColors = () => {
        return  ['FAEEE0', 'F9CF93', 'F0A500', 'FF7777', 'B1E693'];
    }
}