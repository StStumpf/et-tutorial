var app = new Vue({
  el: "#app",
  data() {
    return {
      messages: [],
    };
  },
  mounted() {
    axios.get("http://localhost:3000/messages.json").then((mes) => {
      this.messages = mes.data.map((k) => ({
        ...k,
        timeStamp: `${new Date(k.timeStamp)
          .getHours()
          .toString()
          .padStart(2, "0")}:${new Date(k.timeStamp)
          .getMinutes()
          .toString()
          .padStart(2, "0")}`,
      }));
    });
  },
});

class Message {
  timeStamp;
  messageText;
}
