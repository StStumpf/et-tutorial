const express = require("express");

const app = express();

app.get("/server-time", (request, response) => {
  setTimeout(() => {
    response.send(new Date().toISOString());
  }, 5000);
})

app.use(express.static("html"));

app.listen(3000, () => {
  console.log("Server gestartet. http://localhost:3000");
});
