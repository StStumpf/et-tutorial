const clockContainer = document.getElementById("clock"); 
const stateContainer = document.getElementById("state"); 
const clockStartContainer = document.getElementById("clock-start"); 
const clockEndContainer = document.getElementById("clock-end"); 
const button = document.getElementById("button");
const buttonReset = document.getElementById("button-reset");

refreshClockContainer = () => {
    clockContainer.innerHTML = new Date().toISOString();
}

setState = (state) => {
    stateContainer.innerHTML = state;
}

setStart = (start) => {
    clockStartContainer.innerHTML = start;
}

setEnd = (end) => {
        clockEndContainer.innerHTML = end;
}

let timeout;
let promise;

startPromise = () => {
    setStart(new Date().toISOString());
    setEnd('-');
    setState('pending');

    fetch('http://localhost:3000/server-time')
    .then((content) => {
        setState('fulfilled');
        console.log(content);
        setEnd(new Date().toISOString());
    }).catch((err) => {
        console.log(err);
        setState('rejected');
        setEnd(new Date().toISOString());
    });
}


startPromise();
refreshClockContainer();
setInterval(() => {
    refreshClockContainer();
}, 1000);