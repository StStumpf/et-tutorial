const express = require("express");
const http = require("http");
const WebSocket = require("ws");

const app = express();
const webServer = http.createServer(app);
const webSocketServer = new WebSocket.Server({ server: webServer });

let currentCounter = 0;

app.use(express.static("html"));

sendMessageToAllClients = (message) => {
  webSocketServer.clients.forEach((client) => {
    client.send(message);
  });
};

webSocketServer.on("connection", (ws) => {
  /**
   * Funktion, welche bei einer Nachricht ausgeführt werden soll
   */
   ws.on("message", (message) => {
    currentCounter += 1;
    console.log(`Neue Nachricht per Websocket erhalten: '${message}'`);
    sendMessageToAllClients(JSON.stringify({
      timeStamp: new Date(),
      counter: currentCounter
    }));
  })

  // Verbindung wurde erfolgreich hergestellt. Client Feedback senden.
  ws.send(JSON.stringify({
    timeStamp: new Date(),
    counter: currentCounter
  }));
});

app.listen(3000, () => {
  console.log("Server gestartet. http://localhost:3000");
});

webServer.listen(3001, () => {
  console.log(
    `WebSocket Server wurde gestartet. Adresse = http://localhost:3001`
  );
});
