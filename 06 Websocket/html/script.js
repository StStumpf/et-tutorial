
const counterContainer = document.getElementById("counter"); 
const button = document.getElementById("button"); 

const webSocketUrl = `ws://localhost:3001`;
const ws = new WebSocket(webSocketUrl);

let currentCounter = 0;


refreshCounterContainer = (newCounterValue) => {
    currentCounter = newCounterValue;
    counterContainer.innerHTML = `${currentCounter}`;
}

button.onclick = () => {
    ws.send("Add one to counter, please.")
}

/**
 * Funktion, welche beim Erfolgreichen Öffnen des Websockets abgearbeitet wird
 */
 ws.onopen = function () {
    console.log(`Erfolgreich mit WebSocket verbunden. (URL = ${webSocketUrl})`);
  };
  
  /**
   * Funktion die bei einer neuen Nachricht ausgeführt wird.
   *
   * @param messageEvent Event mit neuer Nachricht.
   */
  ws.onmessage = function (messageEvent) {
    const result = JSON.parse(messageEvent.data);
    const date = new Date(result.timeStamp);
    console.log(`Timestamp`, date.getDate(), date.getMonth(), date.getFullYear());
    // console.log(`Neue Nachricht empfangen`, result);
    refreshCounterContainer(result.counter);
  };



init = () => {}

init();


const colors = {
  RED: 1,
  BLUE: 2
}

const colorMap = {
  1: 'RED',
  2: 'BLUE'
}

console.log(colors.RED);
console.log(colorMap[1]);