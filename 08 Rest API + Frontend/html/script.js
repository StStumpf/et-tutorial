const messagesContainer = document.getElementById("messages");
const button = document.getElementById("button");
const buttonDelete = document.getElementById("button-delete");
const messageInput = document.getElementById("message-text");

let messages = [];

class Message {
  timeStamp;
  messageText;

  /**
   * Nachricht
   * 
   * @param {Date} ts Zeitstempel
   * @param {string} mt Nachrichtinhalt
   */
  constructor(ts, mt) {
    this.timeStamp = new Date(ts);
    this.messageText = mt;
  }

  getHTMLRepresentation() {
    const timestampDiv = `<div class="timestamp">${this.timeStamp.getHours().toString().padStart(2, '0')}:${this.timeStamp.getMinutes().toString().padStart(2, '0')}</div>`;
    const messageTextDiv = `<div class="message-text">${this.messageText}</div>`;

    return `<div class="message">${timestampDiv} ${messageTextDiv}</div>`;
  }
}

/**
 * API METHODS
 */

sendMessageToAPI = (message) => {
  axios
    .post("http://localhost:3000/messages", {
      message: message,
    })
    .then((addedMessage) => {
      messages.push(new Message(addedMessage.data.timeStamp, addedMessage.data.messageText));
      refreshMessageContainer();
    });
};

getAllMessages = () => {
  axios.get("http://localhost:3000/messages").then((mes) => {
    messages = mes.data.map(k => new Message(k.timeStamp, k.messageText));
    refreshMessageContainer();
  });
};

deleteAllMessages = () => {
  axios.delete("http://localhost:3000/messages").then((mes) => {
    messages = mes.data.map(k => new Message(k.timeStamp, k.messageText));
    refreshMessageContainer();
  });
};

/**
 * UI METHODS
 */

refreshMessageContainer = () => {
  if (!messages || !messages.length) {
    messagesContainer.textContent = "Keine Nachrichten gefunden!";
    return;
  }
  messagesContainer.innerHTML = messages
    .map((mes) => mes.getHTMLRepresentation())
    .reduce((x, k) => x + " " + k, "");
};

/**
 * EVENTS
 */

button.addEventListener("click", () => {
  const messageText = messageInput.value.toString();
  if (!messageText) return;

  sendMessageToAPI(messageText);
});

buttonDelete.addEventListener("click", () => {
    deleteAllMessages();
});

/**
 * INIT
 */

getAllMessages();
