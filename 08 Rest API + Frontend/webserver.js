const express = require("express");

const app = express();
app.use(express.json());

let messages = [];

app.get("/messages", (request, response) => {
  response.send(JSON.stringify(messages));
});

app.post("/messages", (request, response) => {
  const messageText = request.body.message.toString();
  messages.push({
    timeStamp: new Date(),
    messageText: messageText
  })
  response.send(JSON.stringify({
    timeStamp: new Date(),
    messageText: messageText
  }));
});

app.delete("/messages", (request, response) => {
  messages = [];
  response.send(JSON.stringify(messages));
});

/**
 * 
 */

app.use(express.static("html"));

app.listen(3000, () => {
  console.log("Server gestartet. http://localhost:3000");
});
