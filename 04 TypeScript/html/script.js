var Playground = /** @class */ (function () {
    function Playground() {
        var _this = this;
        this.colorsChangedCount = 0;
        this.colorPicked = 0;
        this.colorCodes = ['FAEEE0', 'F9CF93', 'F0A500', 'FF7777', 'B1E693'];
        this.refreshClockContainer = function () {
            _this.clockContainer.innerHTML = new Date().toISOString();
        };
        this.refreshColorContainer = function () {
            _this.colorContainer.innerHTML = "#".concat(_this.currentColorCode());
            _this.colorContainer.style.backgroundColor = "#".concat(_this.currentColorCode());
        };
        this.refreshColorChangedCountContainer = function () {
            _this.colorChangedCountContainer.innerHTML = "".concat(_this.colorsChangedCount, " mal");
        };
        this.clockContainer = document.getElementById("clock");
        this.colorContainer = document.getElementById("color");
        this.button = document.getElementById("button");
        this.colorChangedCountContainer = document.getElementById("color-changed-count");
    }
    Playground.prototype.currentColorCode = function () {
        var _a;
        return ((_a = this.colorCodes[this.colorPicked]) !== null && _a !== void 0 ? _a : this.colorCodes[this.colorPicked % (this.colorCodes.length - 1)]);
    };
    Playground.prototype.init = function () {
        var _this = this;
        this.button.onclick = function () {
            _this.colorPicked = (_this.colorPicked + 1) % (_this.colorCodes.length - 1);
            _this.refreshColorContainer();
            _this.colorsChangedCount += 1;
            _this.refreshColorChangedCountContainer();
        };
        this.refreshClockContainer();
        this.refreshColorContainer();
        this.refreshColorChangedCountContainer();
    };
    Playground.prototype.run = function () {
        var _this = this;
        setInterval(function () {
            _this.refreshClockContainer();
        }, 1000);
    };
    return Playground;
}());
var pg = new Playground();
pg.init();
pg.run();
