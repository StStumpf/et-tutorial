class Playground {
  private clockContainer: HTMLElement;
  private colorContainer: HTMLElement;
  private colorChangedCountContainer: HTMLElement;
  private button: HTMLElement;

  private colorsChangedCount = 0;
  private colorPicked = 0;

  private readonly colorCodes = ['FAEEE0', 'F9CF93', 'F0A500', 'FF7777', 'B1E693'];

  private currentColorCode() {
    return (
        this.colorCodes[this.colorPicked] ??
        this.colorCodes[this.colorPicked % (this.colorCodes.length - 1)]
    );
  }

  constructor() {
    this.clockContainer = document.getElementById("clock");
    this.colorContainer = document.getElementById("color");
    this.button = document.getElementById("button");
    this.colorChangedCountContainer = document.getElementById(
      "color-changed-count"
    );
  }

  refreshClockContainer = () => {
    this.clockContainer.innerHTML = new Date().toISOString();
  };

  refreshColorContainer = () => {
    this.colorContainer.innerHTML = `#${this.currentColorCode()}`;
    this.colorContainer.style.backgroundColor = `#${this.currentColorCode()}`;
  };

  refreshColorChangedCountContainer = () => {
    this.colorChangedCountContainer.innerHTML = `${this.colorsChangedCount} mal`;
  };

  init() {
    this.button.onclick = () => {
      this.colorPicked = (this.colorPicked + 1) % (this.colorCodes.length - 1);
      this.refreshColorContainer();
      this.colorsChangedCount += 1;
      this.refreshColorChangedCountContainer();
    };

    this.refreshClockContainer();
    this.refreshColorContainer();
    this.refreshColorChangedCountContainer();
  }

  run() {
    setInterval(() => {
      this.refreshClockContainer();
    }, 1000);
  }
}

const pg = new Playground();
pg.init();
pg.run();
