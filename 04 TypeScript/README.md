Starten der Applikation mit:

```
node webserver.js
```

Konvertieren von TypeScript Dateien:

```
npx tsc in.ts
```

Zur Ausgabe aller Inhalte in eine Datei

```
npx tsc in.ts --outFile out.js
```