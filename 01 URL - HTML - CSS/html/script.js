console.log("Hello World");

const a = {
    firstName: "Stefan",
    lastName: "Stumpf"
};

const jsonString = JSON.stringify(a);
console.log(jsonString);
console.log(jsonString.firstName);

const b = JSON.parse(jsonString);
console.log(b);
console.log(b.firstName);