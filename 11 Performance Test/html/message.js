const messagesContainer = document.getElementById("messages");
const button = document.getElementById("button");
const buttonDelete = document.getElementById("button-delete");
const messageInput = document.getElementById("message-text");

let messages = [];

export class Message {
  timeStamp;
  messageText;

  /**
   * Nachricht
   *
   * @param {Date} ts Zeitstempel
   * @param {string} mt Nachrichtinhalt
   */
  constructor(ts, mt) {
    this.timeStamp = new Date(ts);
    this.messageText = mt;
  }

  getHTMLRepresentation() {
    const timestampDiv = `<div class="timestamp">${this.timeStamp
      .getHours()
      .toString()
      .padStart(2, "0")}:${this.timeStamp
      .getMinutes()
      .toString()
      .padStart(2, "0")}</div>`;
    const messageTextDiv = `<div class="message-text">${this.messageText}</div>`;

    return `<div class="message">${timestampDiv} ${messageTextDiv}</div>`;
  }
}

/**
 * API METHODS
 */

export function getAllMessages() {
  axios.get("http://localhost:3000/messages.json").then((mes) => {
    messages = mes.data.map((k) => new Message(k.timeStamp, k.messageText));

    refreshMessageContainer();
  });
}

/**
 * UI METHODS
 */

export function refreshMessageContainer() {
  if (!messages || !messages.length) {
    messagesContainer.textContent = "Keine Nachrichten gefunden!";
    return;
  }
  messagesContainer.innerHTML = messages
    .map((mes) => mes.getHTMLRepresentation())
    //.reduce((x, k) => x + " " + k, "");
    .join(" ");
}
