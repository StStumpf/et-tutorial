import {getAllMessages} from './message.js';

/**
 * INIT
 */

// Code must wait for finished request
// const longTask = await axios.get("http://localhost:3000/time-consuming-action");
// console.log(longTask.data);

// Better
axios.get("http://localhost:3000/time-consuming-action")
.then(res => {
  const data = res.data;
  console.log(data);
})

getAllMessages();
