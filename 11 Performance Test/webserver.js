const express = require("express");

const app = express();

app.use(express.static("html"));

app.get("/time-consuming-action", (req, res) => {
  const startedAt = new Date();
  setTimeout(() => {
    res.send(
      JSON.stringify({
        start: startedAt,
        end: new Date(),
      })
    );
  }, 10000);
});

app.listen(3000, () => {
  console.log("Server gestartet. http://localhost:3000");
});
